import style from "./index.module.scss";

const LoadingSpinner = () => {
  return (
    <div className={style["c-loading-spinner"]}>
      <div className={style["c-loading-spinner__loader"]} />
    </div>
  );
};

export { LoadingSpinner };
